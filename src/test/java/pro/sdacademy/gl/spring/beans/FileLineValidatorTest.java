package pro.sdacademy.gl.spring.beans;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertTrue;


class FileLineValidatorTest {


    private FileLineValidatorConfiguration configuration;
    private FileLineValidator validator;

    @BeforeEach
    void setUp() {

        configuration = Mockito.mock(FileLineValidatorConfiguration.class);
        Mockito.when(configuration.getCorrectWords()).thenReturn(List.of("Audi"));
        validator = new FileLineValidator(configuration);
    }

    @Test
    void shouldValidateCorrectly(){
        //when
        var result = this.validator.validate("An Audi is a very super car.");

        //then
        assertTrue(result);

    }    @Test
    void shouldValidateIncorrectly(){
        //when
        var result = this.validator.validate("An Audi is a very super car.");

        //then
        assertTrue(result);
    }
}