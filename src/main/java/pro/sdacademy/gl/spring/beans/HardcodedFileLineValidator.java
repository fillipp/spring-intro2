package pro.sdacademy.gl.spring.beans;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Primary
@Component
public class HardcodedFileLineValidator implements LineValidator{

    @Override
    public boolean validate(String line) {
        return line.contains("correct");
    }
}
