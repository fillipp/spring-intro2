package pro.sdacademy.gl.spring.beans;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;

@Component
public class TextFileParser {

    private final LineValidator validator;

    public TextFileParser(@Qualifier("hardcodedFileLineValidator") LineValidator validator) {
        this.validator = validator;
    }

    public void parse(Resource file) {
        try (var stream = Files.newInputStream(file.getFile().getAbsoluteFile().toPath());
             var reader = new BufferedReader(new InputStreamReader(stream))){

            reader.lines()
                    .filter(line->this.validator.validate(line))
                    .forEach(line-> System.out.println("Line is correct: " + line));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
