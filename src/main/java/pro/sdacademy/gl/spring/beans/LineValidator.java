package pro.sdacademy.gl.spring.beans;

public interface LineValidator {
    boolean validate(String line);
}
