package pro.sdacademy.gl.spring.beans;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class FileLineValidator implements LineValidator{

    private final FileLineValidatorConfiguration configuration;

    @Override
    public boolean validate(String line){
        return this.configuration.getCorrectWords().stream().anyMatch(word->line.contains(word));
    }
}
